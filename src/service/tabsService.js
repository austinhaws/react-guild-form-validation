import TabFields from "../enum/TabFields";

const tabService = {
    /**
     * @param {object} errors : key is the field name, value is an array of errors or an empty array if no errors; Can get the error object from useValidationErrors()
     * @param {string} tab : the tab for the fields to check for errors
     * @returns how many fields have errors
     */
    numTabErrors: ({ errors, tab }) => {
        const currentFields = TabFields[tab];
        const fieldErrors = currentFields.map(field => errors[field])
            .filter(errorArray => errorArray && errorArray.length);

        return fieldErrors.length;
    },
};

export default tabService;
