const dirtyService = {
    setCleanEmployee: employee => ({...employee}),
};

export default dirtyService;
