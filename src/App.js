import './App.css';
import Form from './components/Form/Form';
import "./css/app.css";

const propTypes = {};
const defaultProps = {};

const App = () => {
    return (
        <div className="App">
            <h1>May the Form be with you!</h1>
            <Form/>
        </div>
    );
}

App.propTypes = propTypes;
App.defaultProps = defaultProps;

export default App;
