import split from 'lodash/split';

export default (baseObject, path) => split(path, '\.').reduce((obj, field) => (field && obj) ? obj[field] : obj, baseObject);
