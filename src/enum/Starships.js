const Starships = [
    {
        name: 'Yacht',
        description: 'A luxurious vessel typically owned by a politician, diplomat or rich person.',
    },
    {
        name: 'Starfighter— A small one- or two',
        description: 'A vessel typically used for dog fighting.',
    },
    {
        name: 'Bomber',
        description: 'A slower ship armed with bombs.',
    },
    {
        name: 'Scout vessel',
        description: 'A small, fast ship used to scout an area before a main force arrives.',
    },
    {
        name: 'Transport',
        description: 'A cargo or passenger carrier.',
    },
    {
        name: 'Shuttle',
        description: 'A small craft used to transport personnel, usually at least partially though space.',
    },
    {
        name: 'Gunship',
        description: 'Used for atmospheric or space combat.',
    },
    {
        name: 'System patrol craft— Used for in',
        description: 'System operations; usually lacked hyper drives.',
    },
    {
        name: 'Freighter',
        description: 'A cargo or freight carrier.',
    },
    {
        name: 'Capital ship',
        description: 'The largest of starships, often used as a warship.',
    },
    {
        name: 'Space station',
        description: 'An artificial structure for use in orbit or deep space.',
    },
];

export default Starships;
