const Tabs = {
    personal: "Personal",
    contact: "Contact",
    career: "Career",
    starship: "Starship",
    submit: "Submit",
};

export default Tabs;
