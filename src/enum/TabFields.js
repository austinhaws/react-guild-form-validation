import Fields from "./Fields";
import Tabs from "./Tabs";

const TabFields = {
    [Tabs.personal]: [
        Fields.firstName,
        Fields.middleName,
        Fields.lastName,
        Fields.age,
        Fields.birthDate,
        Fields.race,
        Fields.isSith,
        Fields.isJedi,
        Fields.ssn,
    ],

    [Tabs.contact]: [
        Fields.email,
        Fields.addressLine1,
        Fields.addressLine2,
        Fields.city,
        Fields.sector,
        Fields.planet,
        Fields.planetCoordinates,
    ],

    [Tabs.career]: [
        Fields.job,
        Fields.creditsPerHour,
        Fields.employer,
        Fields.employmentStartDate,
        Fields.webUrl,
    ],

    [Tabs.starship]: [
        Fields.starshipName,
        Fields.starshipType,
        Fields.starshipArmament,
        Fields.starshipCrewSize,
        Fields.starshipLength,
        Fields.starshipParsecsForKesselRun,
    ],

    [Tabs.submit]: [
        Fields.signature,
        Fields.agreeTermsOfUse,
        Fields.notDroid,
    ],
};

export default TabFields;
