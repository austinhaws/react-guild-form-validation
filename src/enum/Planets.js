const Planets = [
    {
        name: 'Abafar',
        decription: 'A desert planet located in the Outer Rim with a completely white surface. Known as The Void, the planet is barely populated but is home to massive amounts of rhydonium, a scarce and volatile fuel.',
        type: 'planet',
    },
    {
        name: 'Agamar',
        decription: 'A barren, rocky planet in the Outer Rim. Site of a still-active reserve of Separatist droids that became known for surviving the Clone Wars.',
        type: 'planet',
    },
    {
        name: 'Ahch-To',
        decription: 'Oceanic planet on which Luke Skywalker has been hiding for several years, and the location of the first Jedi Temple and the Tree Library of sacred Jedi texts. Porgs and thala-sirens are among the native species.',
        type: 'planet',
    },
    {
        name: 'Ajan Kloss',
        decription: 'A jungle moon which serves as a new base of operations for the Resistance. Leia Organa dies here after using the last of her energy to reach out to her son through the Force.',
        type: 'moon',
    },
    {
        name: 'Akiva',
        decription: 'Jungle planet and home of Norra and Temmin Wexley, and a primary setting in Aftermath.',
        type: 'planet',
    },
    {
        name: 'Alderaan',
        decription: 'Forests, mountains; home planet of Princess Leia and the House of Organa. Destroyed by the first Death Star as punishment for involvement in the Rebel Alliance and also as a demonstration of power.',
        type: 'planet',
    },
    {
        name: 'Aleen',
        decription: 'A subterranean world located in the Mid Rim. The native species include, the surface dwelling beings called the Aleena and sentient tree-like beings called Kindalo.',
        type: 'moon',
    },
    {
        name: 'Alzoc III',
        decription: 'Ice planet, with many glaciers, and in its surface lies a crashed separatist carrier, with an anti-gravitational device.',
        type: 'planet',
    },
    {
        name: 'Anaxes',
        decription: 'A rocky planet located in the Azure sector of the galaxy\'s Core Worlds region.A majority of its surface was covered with tall, red plant- like life.In the early years of the Imperial Era, the planet was destroyed in a cataclysm, with the remains becoming the Anaxes asteroid belt.',
        type: 'planet',
    },
    {
        name: 'Ando',
        decription: 'A water-covered planet located in the Outer Rim, it is home to the amphibious Aqualish species and endures great turmoil during the Separatist Crisis.',
        type: 'planet',
    },
    {
        name: 'Anoat',
        decription: 'Polluted; A nearly inhospitable planet used by the Empire.',
        type: 'planet',
    },
    {
        name: 'Atollon',
        decription: 'Desert planet, site of Phoenix Squadron Rebel base. Home of the spider-like hexapods known as the Krykna, and the powerful force-wielder Bendu.',
        type: 'planet',
    },
    {
        name: 'Balnab',
        decription: 'A Barbaric world once visited by R2-D2 and C-3PO.',
        type: 'moon',
    },
    {
        name: 'Batuu',
        decription: 'A remote frontier outpost and old trading port',
        type: 'moon',
    },
    {
        name: 'Bespin',
        decription: 'A gas planet with a thin layer of habitable atmosphere where Cloud City was located. The planet\'s gas layers were a source of rare tibanna gas which was harvested and refined at mining complexes like Cloud City.',
        type: 'planet',
    },
    {
        name: 'Bracca',
        decription: 'An inhospitable junkyard planet in the Mid Rim considered strategically important in the Clone Wars. Becomes useful to the Scrapper Guild whose members salvage decommissioned ships for the Empire.',
        type: 'planet',
    },
    {
        name: 'Cantonica',
        decription: 'An arid planet where the resort city of Canto Bight, home of the Canto Casino and Racetrack, is located.',
        type: 'planet',
    },
    {
        name: 'Castilon',
        decription: 'An ocean planet located in the Outer Rim near Wild Space. Home to the aircraft refueling station Colossus, as well as a destination for racers.',
        type: 'planet',
    },
    {
        name: 'Cato Neimoidia',
        decription: 'The site of battles throughout the Clone Wars, notable for its "Bridge Cities". Also the site of Plo Koon\'s death during the Great Jedi Purge.',
        type: 'moon',
    },
    {
        name: 'Chandrila',
        decription: 'Homeworld of Mon Mothma, it serves as the first capital of the New Republic. Serene planet known for calm seas and rolling hills.',
        type: 'planet',
    },
    {
        name: 'Christophsis',
        decription: 'During the Clone Wars, the Battle of Christophsis occurs here, serving as an introduction to Ahsoka Tano.',
        type: 'moon',
    },
    {
        name: 'Concord Dawn',
        decription: 'Home planet of Jango Fett. Habitable planet that is surrounded by a large amount of debris from many vicious wars. Formerly controlled by the Mandalorians.',
        type: 'planet',
    },
    {
        name: 'Corellia',
        decription: 'Industrial planet located in the Core of the galaxy, with a strong culture of training pilots. Homeworld of Han Solo.',
        type: 'planet',
    },
    {
        name: 'Coruscant',
        decription: 'Cosmopolitan urban world consisting of one planet-wide city. Governmental center of the Galactic Republic and later the Galactic Empire.',
        type: 'planet',
    },
    {
        name: 'Crait',
        decription: 'Small mineral planet located in a remote section of the galaxy, its surface is covered with a layer of white salt over its red-colored soil. In Leia, Princess of Alderaan it is the location of an early Rebel Alliance outpost. Leia and her remaining Resistance forces flee there in The Last Jedi, where they face off with the First Order.',
        type: 'planet',
    },
    {
        name: 'D\'Qar',
        decription: 'Site of a Resistance operations base led by General Leia Organa.',
        type: 'moon',
    },
    {
        name: 'Dagobah',
        decription: 'Swamp planet and Yoda\'s residence after the fall of the Jedi.',
        type: 'planet',
    },
    {
        name: 'Dantooine',
        decription: 'Rural planet and the former site of a Rebel base. The writers of Rogue One considered depicting the Rebels evacuating the base for Yavin 4, but "it didn\'t move the story forward and it would have cost a ton of money."',
        type: 'planet',
    },
    {
        name: 'Dathomir',
        decription: 'Homeworld of the Force-sensitive Nightsisters, including Asajj Ventress. Also the homeworld of the Zabrak warriors, including Darth Maul.',
        type: 'moon',
    },
    {
        name: 'Devaron',
        decription: 'Forest planet with an ancient Jedi Temple.',
        type: 'planet',
    },
    {
        name: 'Eadu',
        decription: 'Rocky, mountainous planet beset by constant severe storms. Home to an Imperial weapons research facility. Its appearance was partly inspired by the fictitious planet LV-426 from the Alien franchise.',
        type: 'planet',
    },
    {
        name: 'Endor',
        decription: 'Blue gas giant with a complex planetary system, including moons like Sanctuary and Kef Bir, this system was controlled by the Empire until the second Death Star was destroyed',
        type: 'planet',
    },
    {
        name: 'Endor (Sanctuary)',
        decription: 'Forest moon that the second Death Star orbits, and house to an Imperial outpost. Inhabited by Ewoks. The location of the battle between the Rebel Alliance and the Empire leading to the destruction of the second Death Star.',
        type: 'moon',
    },
    {
        name: 'Er´kit',
        decription: 'Desert planet located in the galaxy\'s Outer Rim Territories.Homeworld of the Er\'Kit species.',
        type: 'planet',
    },
    {
        name: 'Eriadu',
        decription: 'Planet located in the galaxy\'s Outer Rim.Homeworld of Grand Moff Tarkin and his family.',
        type: 'planet',
    },
    {
        name: 'Esseles',
        decription: 'Under Imperial Control. The Empire has a listening post concealed in Esseles icy ring.',
        type: 'moon',
    },
    {
        name: 'Exegol',
        decription: 'A stormy planet in the galaxy\'s "Unknown Regions".An ancient bastion of the Sith, it serves as the lair of Darth Sidious and the Sith Eternal during the construction of his armada known as the Final Order.',
        type: 'planet',
    },
    {
        name: 'Felucia',
        decription: 'Jungle planet teeming with plants but little animal life. Aayla Secura is assassinated here during the Jedi Purge.',
        type: 'planet',
    },
    {
        name: 'Florrum',
        decription: 'Sulfurous desert planet. Hondo Ohnaka is the leader of a pirate gang based on the planet.',
        type: 'planet',
    },
    {
        name: 'Fondor',
        decription: 'Imperial manufacturing center with large shipyards.',
        type: 'moon',
    },
    {
        name: 'Geonosis',
        decription: 'Rocky desert planet where battle droids are manufactured, and the site of the opening battle of the Clone Wars. All life on the planet is presumed destroyed by the Empire in Star Wars Rebels, with two exceptions: Klik-Klak and his offspring. Primary construction site of the first Death Star. Homeworld of the Geonosians.',
        type: 'planet',
    },
    {
        name: 'Hosnian Prime',
        decription: 'Urban planet and capital of the New Republic. Destroyed by the First Order\'s Starkiller Base.',
        type: 'planet',
    },
    {
        name: 'Hoth',
        decription: 'Desolate ice planet and base for the Rebel Alliance.',
        type: 'planet',
    },
    {
        name: 'Iego',
        decription: 'An Outer Rim planet which harbors flying creatures called xandu and medicinally important plants called reeska. Iego is surrounded by a 1000 moons and at least one of these, named Millius Prime, is home to a race called the Angels.',
        type: 'planet',
    },
    {
        name: 'Ilum',
        decription: 'Remote ice planet where kyber crystals are mined. It became Starkiller Base.',
        type: 'planet',
    },
    {
        name: 'Iridonia',
        decription: 'Home of the Iridonian people and rumored birthplace of Darth Maul.',
        type: 'moon',
    },
    {
        name: 'Jakku',
        decription: 'Desert planet. Site of a "graveyard" of ships damaged during the Battle of Jakku, the final battle between the New Republic and the Galactic Empire. Also the homeworld of Rey.',
        type: 'planet',
    },
    {
        name: 'Jedha',
        decription: 'Cold desert moon, and a sacred place for believers in the Force. A source of kyber crystals used for lightsabers and the Death Star\'s primary weapon.It is also the first location on which the Death Star\'s destructive capability is tested. Rogue One\'s director, Gareth Edwards, has described the location as "a Mecca or Jerusalem within the Star Wars world".It is also a homonym for Jeddah, the principal gateway to Mecca.',
        type: 'moon',
    },
    {
        name: 'Jestefad',
        decription: 'A large gas planet that is part of the Mustafar system.',
        type: 'planet',
    },
    {
        name: 'Kamino',
        decription: 'Ocean planet where cloning technology is developed and the Clone Army is created and trained. Obi-Wan Kenobi discovers that the planet is missing from the Jedi archives; it is later revealed to have been deleted as a part of Darth Sidious\' plot to start the Clone Wars.',
        type: 'planet',
    },
    {
        name: 'Kashyyyk',
        decription: 'Forest planet and home of the Wookiees. Also the site of one of the final battles of the Clone Wars.',
        type: 'planet',
    },
    {
        name: 'Kef Bir',
        decription: 'A moon of Endor and the location of some of the wreckage of the second Death Star after it was destroyed in Return of the Jedi.',
        type: 'moon',
    },
    {
        name: 'Kessel',
        decription: 'A mining planet that has been fought over by crime lords for its valuable Spice. A fissure vent beneath the spice mines served as a source of astatic coaxium, an element that could be refined into hyperfuel for starships.',
        type: 'planet',
    },
    {
        name: 'Kijimi',
        decription: 'A frigid mountainous planet. It was the homeworld of Zorii Bliss, leader of the smuggling gang known as the Spice Runners of Kijimi. It is later destroyed by the Sith Eternal as a show of force for the rest of the galaxy.',
        type: 'planet',
    },
    {
        name: 'Kuat',
        decription: 'Industrial planet home to Kuat Drive Yards, the manufacturer of Star Destroyers.',
        type: 'planet',
    },
    {
        name: 'Lah\'mu',
        decription: 'A remote planet with black sands, where Jyn Erso and her parents go into hiding.',
        type: 'planet',
    },
    {
        name: 'Lothal',
        decription: 'Remote farm planet and birthplace of Ezra Bridger.',
        type: 'planet',
    },
    {
        name: 'Lotho Minor',
        decription: 'A planetary junkyard and hiding place of Darth Maul after his presumed death.',
        type: 'planet',
    },
    {
        name: 'Malachor',
        decription: 'Desolate Sith temple world and site of two major battles thousands of year apart: one involving the Scourge of Malachor, the other between Darth Maul, several Rebels, Darth Vader, and several Inquisitors.',
        type: 'moon',
    },
    {
        name: 'Malastare',
        decription: 'Forested planet where podracing is popular. Birthplace of Sebulba and homeworld of the Dug.',
        type: 'planet',
    },
    {
        name: 'Mandalore',
        decription: 'Outer Rim planet that is the homeworld of the Mandalorians torn by wars between Mandalorians and Jedi and eventually purged by the Empire scattering the few Mandalorians throughout the galaxy (including Bo-Katan).',
        type: 'planet',
    },
    {
        name: 'Maridun',
        decription: 'Grassy planet remaining undiscovered until the Clone Wars.',
        type: 'planet',
    },
    {
        name: 'Mimban',
        decription: 'Swamp planet with perpetual fog and overcast sky.',
        type: 'planet',
    },
    {
        name: 'Mon Cala',
        decription: 'Ocean planet, home to the Mon Calamari and Quarren species. Also known as Mon Calamari, or Dac.',
        type: 'planet',
    },
    {
        name: 'Moraband',
        decription: 'Home planet of ancient Sith lords. Known as Korriban in Legends.',
        type: 'planet',
    },
    {
        name: 'Mortis',
        decription: 'Planet with a wall surrounding it, home to the three omnipotent Force wielders known only in the Jedi Archives as the Mortis Gods.',
        type: 'planet',
    },
    {
        name: 'Mustafar',
        decription: 'Volcanic planet used by the Techno Union to mine for valuable materials. Hiding place of the Separatist Council during the final days of the Clone Wars, and the location of the duel between Anakin Skywalker and Obi-Wan Kenobi that results in the former\'s disfigurement.During the Imperial Era, it is the location of Darth Vader\'s personal stronghold. In the VR game Vader Immortal, it is revealed that Mustafar was once forested and very populous, but was left devastated by an ancient battle. It later begins to slowly heal itself, and, by the time of The Rise of Skywalker, trees appear to have regrown on Mustafar\'s surface.',
        type: 'planet',
    },
    {
        name: 'Mygeeto',
        decription: 'Cold, urban planet that served as a battle site in the final days of the Clone Wars, as well as the death place of Jedi Master Ki Adi Mundi.',
        type: 'planet',
    },
    {
        name: 'Naboo',
        decription: 'Home planet of the Gungans, including Jar-Jar Binks, and various humans, who compose a civilization called the Naboo, which includes Padmé Amidala and Emperor Palpatine.',
        type: 'planet',
    },
    {
        name: 'Nal Hutta',
        decription: 'Home planet of Jabba and other Hutts. Close to the urban moon of Nar Shaddaa.',
        type: 'planet',
    },
    {
        name: 'Nevarro',
        decription: 'A volcanic planet with black sands and lava flows, located in the Outer Rim Territories. Nevarro was a hub of the Bounty Hunter Guild as well as home to a Mandalorian covert in the early days of the New Republic.',
        type: 'planet',
    },
    {
        name: 'Numidian Prime',
        decription: 'Rainforest paradise that is a haven to smugglers and thieves.',
        type: 'moon',
    },
    {
        name: 'Onderon',
        decription: 'Jungle planet where Anakin Skywalker leads a revolt against its monarchy; birthplace of Saw Gerrera.',
        type: 'planet',
    },
    {
        name: 'Ord Mantell',
        decription: 'A planet where Han Solo tells of having a run-in with a bounty hunter.',
        type: 'planet',
    },
    {
        name: 'Pasaana',
        decription: 'A desert planet on the edge of the galaxy, home to the Aki-Aki species.',
        type: 'planet',
    },
    {
        name: 'Pillio',
        decription: 'Uncolonized aquatic planet with over 3 million species, and the location of one of Palpatine\'s observatories.',
        type: 'planet',
    },
    {
        name: 'Polis Massa',
        decription: 'Outer Rim planetoid within an asteroid field of the same name; birthplace of Luke Skywalker and Leia Organa.',
        type: 'planet',
    },
    {
        name: 'Rishi',
        decription: 'Tropical planet used by the Republic to monitor the nearby cloning facility on Kamino.',
        type: 'planet',
    },
    {
        name: 'Rodia',
        decription: 'Home planet of the Rodians, including Greedo. A remote swampy, jungle planet, it was represented by Onaconda Farr in the Galactic Senate during the Clone Wars.',
        type: 'planet',
    },
    {
        name: 'Rugosa',
        decription: 'A moon that consists of giant coral reefs. Yoda meets King Katuunko of the Toydarians here.',
        type: 'moon',
    },
    {
        name: 'Ruusan',
        decription: 'Barren planet housing the Valley of the Jedi. Site of a great battle between the Sith and the Jedi.',
        type: 'planet',
    },
    {
        name: 'Ryloth',
        decription: 'Dry, hot home planet of the Twi\'leks, including Hera Syndulla and Jedi Master Aayla Secura.',
        type: 'planet',
    },
    {
        name: 'Saleucami',
        decription: 'Primary terrain deserts and swamps. Home of the Clone Trooper deserter Cut Lawquane. Jedi Master Stass Allie is killed here during the Great Jedi Purge.',
        type: 'moon',
    },
    {
        name: 'Savareen',
        decription: 'Desert and ocean planet. where destitute villages farm wind and refine coaxium. In Solo: A Star Wars Story, the Millennium Falcon arrives there after Han and his crew steal coaxium from Kessel, and Dryden Vos and Tobias Beckett die there.',
        type: 'planet',
    },
    {
        name: 'Scarif',
        decription: 'Oceanic "paradise world" used for construction of the Death Star after Geonosis. When Rebel Alliance members raid the Imperial database from the secret base on one of its tropical islands, the planet is destroyed to impede their escape with the Death Star plans.',
        type: 'planet',
    },
    {
        name: 'Serenno',
        decription: 'A planet in the Outer Rim Territories. Homeworld of Count Dooku.',
        type: 'planet',
    },
    {
        name: 'Shili',
        decription: 'Home planet of the Togruta, including Jedi Council member Shaak Ti and Ahsoka Tano.',
        type: 'planet',
    },
    {
        name: 'Sissubo',
        decription: 'Seventh Planet of the Chandrila system. It is surrounded by a debris field of remnants of Imperial Ships.',
        type: 'planet',
    },
    {
        name: 'Skako Minor',
        decription: 'Home planet of Wat Tambor.',
        type: 'planet',
    },
    {
        name: 'Sorgan',
        decription: 'A forested backwater planet in the Outer Rim mostly populated by human farmers who harvest krill which is used to make spotchka, a popular drink on the planet. Local farmers are constantly attacked by Klatooinians.',
        type: 'planet',
    },
    {
        name: 'Starkiller Base',
        decription: 'Snowy planet of forested mountains converted by the First Order into a superweapon. Later destroyed by the Resistance and gave birth to a miniature star, which was named "Solo" as an homage to Han Solo, who was killed by his son Kylo Ren on the planet. It is the converted version of Ilum.',
        type: 'planet',
    },
    {
        name: 'Subterrel',
        decription: 'Mining planet mentioned by Dexter Jettster who spent time prospecting there. Located near Kamino, beyond the Outer Rim.',
        type: 'planet',
    },
    {
        name: 'Sullust',
        decription: 'A volcanic planet whose atmosphere was highly toxic forcing the native Sullustans to build technologically advanced subterranean cities. It was the base of Imperial factories and the SoroSuub corporation employed roughly half the population.',
        type: 'planet',
    },
    {
        name: 'Takodana',
        decription: 'Forest planet and site of Maz Kanata\'s castle.Neutral territory between First Order and Resistance.',
        type: 'planet',
    },
    {
        name: 'Tatooine',
        decription: 'Desert planet and childhood home of Anakin Skywalker and Luke Skywalker. Location of Jabba the Hutt\'s palace.',
        type: 'planet',
    },
    {
        name: 'Teth',
        decription: 'During the Clone Wars, Jabba\'s son Rotta is abducted by the Separatists and brought to this planet.Anakin and Ahsoka Tano travel to Teth and rescue Rotta from Asajj Ventress.',
        type: 'planet',
    },
    {
        name: 'Toydaria',
        decription: 'Home planet of Watto and other Toydarians. Close to Nal Hutta.',
        type: 'planet',
    },
    {
        name: 'Trandosha',
        decription: 'Homeworld of the Trandoshan hunters. Close to Kashyyyk.',
        type: 'moon',
    },
    {
        name: 'Umbara',
        decription: 'Planet with a thick, foggy atmosphere. Home to the Umbarans.',
        type: 'planet',
    },
    {
        name: 'Utapau',
        decription: 'Remote planet, covered in deep sinkholes and home to the Utai and Pau\'ans. Site of General Grievous\' defeat and a separatist base during the last days of the Clone Wars.',
        type: 'planet',
    },
    {
        name: 'Vandor-1',
        decription: 'Icy, mountainous planet that is the site of a Crimson Dawn train heist led by Tobias Beckett in Solo: A Star Wars Story.',
        type: 'planet',
    },
    {
        name: 'Vardos',
        decription: 'Imperial stronghold and home to Iden and Garrick Versio. One of the first targets of Operation: Cinder.',
        type: 'moon',
    },
    {
        name: 'Wobani',
        decription: 'A desolate wasteland and the site of an Imperial penal labor colony.',
        type: 'moon',
    },
    {
        name: 'Wrea',
        decription: 'A planet near an asteroid field where Saw Gerrara raises Jyn Erso after her father is taken by the Empire.',
        type: 'planet',
    },
    {
        name: 'Yavin',
        decription: 'Gas planet with several moons, including Yavin 4.',
        type: 'planet',
    },
    {
        name: 'Yavin 4',
        decription: 'Forest moon and base for the Rebel Alliance.',
        type: 'moon',
    },
    {
        name: 'Zeffo',
        decription: 'A planet with many mountains and stormy weather. Featuring ancient ruins and tombs, imperial excavations, and a crashed Venator near an ancient tomb.',
        type: 'planet',
    },
    {
        name: 'Zygerria',
        decription: 'A planet in the Outer Rim Territories home of the Zygerrian species. Zygerria was also known for a slave empire.',
        type: 'planet',
    },

];

export default Planets;
