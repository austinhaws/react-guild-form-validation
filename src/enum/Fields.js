const Fields = {
    firstName: 'firstName',
    middleName: 'middleName',
    lastName: 'lastName',
    age: 'age',
    birthDate: 'birthDate',
    race: 'race',
    isSith: 'isSith',
    isJedi: 'isJedi',
    ssn: 'ssn',

    // contact information
    email: 'email',
    addressLine1: 'addressLine1',
    addressLine2: 'addressLine2',
    city: 'city',
    sector: 'sector',
    planet: 'planet',
    planetCoordinates: 'planetCoordinates',

    // career information
    job: 'job',
    creditsPerHour: 'creditsPerHour',
    employer: 'employer',
    employmentStartDate: 'employmentStartDate',
    webUrl: 'webUrl',

    // starship information
    starshipName: 'starship.name',
    starshipType: 'starship.type',
    starshipArmament: 'starship.armament',
    starshipCrewSize: 'starship.crewSize',
    starshipLength: 'starship.length',
    starshipParsecsForKesselRun: 'starship.parsecsForKesselRun',

    // bookkeeping
    signature: 'signature',
    agreeTermsOfUse: 'agreeTermsOfUse',
    notDroid: 'notDroid',
};

export default Fields;
