import PropTypes from "prop-types";
import useAppDataContext from "../../AppDataProvider/hooks/useAppDataContext";
import useValidationErrors from "../../AppDataProvider/hooks/useValidationErrors";
import objectAtPath from "../../util/objectAtPath";
import './LabelInput.css';

const propTypes = {
    field: PropTypes.string,
    label: PropTypes.string.isRequired,
};
const defaultProps = {
    field: null,
};

const LabelInput = ({ field, label }) => {
    const errors = useValidationErrors(field);
    const { onFieldChange, state: { employee } } = useAppDataContext();

    return (
        <div className="label-input__container">
            <div className="label-input__label">{label}:</div>
            <div className="label-input__value">
                <input
                    className={(errors && errors.length) ? 'label-input__value--error' : ''}
                    type="text"
                    value={objectAtPath(employee, field)}
                    onChange={e => onFieldChange(field, e.target.value)}
                />
            </div>
            {
                (errors && errors.length)
                    ? (
                        <div className="label-input__error">
                            {errors.join(', ')}
                        </div>
                    )
                    : undefined
            }
        </div>
    );
}

LabelInput.propTypes = propTypes;
LabelInput.defaultProps = defaultProps;

export default LabelInput;
