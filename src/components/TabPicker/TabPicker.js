import produce from 'immer';
import PropTypes from "prop-types";
import useAppDataContext from '../../AppDataProvider/hooks/useAppDataContext';
import useValidationErrors from '../../AppDataProvider/hooks/useValidationErrors';
import tabsService from '../../service/tabsService';
import './TabPicker.css';

const propTypes = {
    currentTab: PropTypes.string.isRequired,
    tabs: PropTypes.arrayOf(PropTypes.string).isRequired,
};
const defaultProps = {};

const TabPicker = ({ currentTab, tabs }) => {
    const { setState } = useAppDataContext();
    const errors = useValidationErrors();

    // TODO: set current tab state when clicked
    return (
        <div className="tab-picker__container">
            {
                tabs.map((tab, i, a) => (
                    <div
                        onClick={() => setState(produce(draftState => void (draftState.formState.formDetailTab = tab)))}
                        key={tab}
                        className={
                            `tab-picker__tab ` +
                            (tab === currentTab ? 'tab-picker__tab--current-tab' : '') + ' ' +
                            ((i === a.length - 1) ? 'tab-picker__tab--last-tab' : '')
                        }
                    >
                        {/* TODO: Show # of validation errors in a circle for this tab */}
                        {tab} ({tabsService.numTabErrors({ errors, tab })})
                    </div>
                ))
            }
        </div>
    );
};

TabPicker.propTypes = propTypes;
TabPicker.defaultProps = defaultProps;

export default TabPicker;
