import "./FormDetail.css";
import useAppDataContext from "../../AppDataProvider/hooks/useAppDataContext";
import Tabs from "../../enum/Tabs";
import TabPicker from "../TabPicker/TabPicker";
import FormDetailCareer from "./FormDetailCareer";
import FormDetailContact from "./FormDetailContact";
import FormDetailPersonal from "./FormDetailPersonal";
import FormDetailStarship from "./FormDetailStarship";
import FormDetailSubmit from "./FormDetailSubmit";

const propTypes = {};
const defaultProps = {};

const FormDetail = () => {
    const { state: { formState: { formDetailTab } = {} } } = useAppDataContext();

    const DetailComponent = {
        [Tabs.personal]: FormDetailPersonal,
        [Tabs.contact]: FormDetailContact,
        [Tabs.career]: FormDetailCareer,
        [Tabs.starship]: FormDetailStarship,
        [Tabs.submit]: FormDetailSubmit,

    }[formDetailTab];

    return (
        <div className="form-detail">
            <TabPicker currentTab={formDetailTab} tabs={Object.values(Tabs)} />
            <DetailComponent />
        </div>
    );
}

FormDetail.propTypes = propTypes;
FormDetail.defaultProps = defaultProps;

export default FormDetail;
