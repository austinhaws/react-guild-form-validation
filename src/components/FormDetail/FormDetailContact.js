import LabelInput from "../LabelInput/LabelInput";

const propTypes = {};
const defaultProps = {};

const FormDetailContact = () => {

    return (
        <div className="form-detail-contact form-detail-container">
            {/* TODO: Validation: isEmail */}
            <LabelInput field="email" label="E-Mail" />

            {/* TODO: Validation: none? */}
            <LabelInput field="addressLine1" label="Address Line 1" />

            {/* TODO: Validation: none? */}
            <LabelInput field="addressLine2" label="Address Line 2" />

            {/* TODO: Validation: none? */}
            <LabelInput field="city" label="City" />

            {/* TODO: Validation: isUpperCase */}
            <LabelInput field="sector" label="Sector" />

            {/* TODO: Validation: isInArray - see Planets enum*/}
            <LabelInput field="planet" label="Planet/Moon" />

            {/* TODO: Validation: regex match: ###.###.### */}
            <LabelInput field="planetCoordinates" label="Planet Coordinates" />
        </div>
    );
}

FormDetailContact.propTypes = propTypes;
FormDetailContact.defaultProps = defaultProps;

export default FormDetailContact;
