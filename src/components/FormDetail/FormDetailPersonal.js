import LabelInput from "../LabelInput/LabelInput";

const propTypes = {};
const defaultProps = {};

const FormDetailPersonal = () => {
    return (
        <div className="form-detail-personal form-detail-container">
            {/* TODO: Validation: Required (not empty), alpha numeric */}
            <LabelInput field="firstName" label="First Name" />

            {/* TODO: Validation: alpha */}
            <LabelInput field="middleName" label="Middle Name" />

            {/* TODO: Validation: Required (not empty), alpha numeric */}
            <LabelInput field="lastName" label="Last Name" />

            {/* TODO: Validation: isInt, matches birth date */}
            <LabelInput field="age" label="Age" />

            {/* TODO: Validation: isDate, required (not empty), matches birth date */}
            <LabelInput field="birthDate" label="Birth Date" />

            {/* TODO: Validation: isInArray - see Races enum */}
            <LabelInput field="race" label="Race" />

            {/* TODO: Validation: isBool */}
            <LabelInput field="isSith" label="Is Sith" />

            {/* TODO: Validation: isBool */}
            <LabelInput field="isJedi" label="Is Jedi" />

            {/* TODO: Validation: isSSN */}
            <LabelInput field="ssn" label="SSN (senate serial number)" />
        </div>
    );
}

FormDetailPersonal.propTypes = propTypes;
FormDetailPersonal.defaultProps = defaultProps;

export default FormDetailPersonal;
