import LabelInput from "../LabelInput/LabelInput";

const propTypes = {};
const defaultProps = {};

const FormDetailCareer = () => {

    return (
        <div className="form-detail-contact form-detail-container">
            {/* TODO: None? */}
            <LabelInput field="job" label="Job Title" />

            {/* TODO: isCurrency */}
            <LabelInput field="creditsPerHour" label="Credits Per Hour" />

            {/* TODO: None? */}
            <LabelInput field="employer" label="Employer" />

            {/* TODO: isDate */}
            <LabelInput field="employmentStartDate" label="Employment Start Date" />

            {/* TODO: isUrl */}
            <LabelInput field="webUrl" label="Web URL" />

        </div>
    );
}

FormDetailCareer.propTypes = propTypes;
FormDetailCareer.defaultProps = defaultProps;

export default FormDetailCareer;
