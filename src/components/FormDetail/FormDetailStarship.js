import LabelInput from "../LabelInput/LabelInput";

const propTypes = {};
const defaultProps = {};

const FormDetailStarship = () => {

    return (
        <div className="form-detail-contact form-detail-container">
            {/* TODO: None? */}
            <LabelInput field="starship.name" label="Name" />

            {/* TODO: isInArray - see Starship enum */}
            <LabelInput field="starship.type" label="Type" />

            {/* TODO: Contains a comma (,): ie Comma delimited */}
            <LabelInput field="starship.armament" label="Armament" />

            {/* TODO: isNumeric */}
            <LabelInput field="starship.crewSize" label="Crew Size" />

            {/* TODO: isNumeric */}
            <LabelInput field="starship.length" label="Length" />

            {/* TODO: isDecimal */}
            <LabelInput field="starship.parsecsForKesselRun" label="Parsecs For Kessel Run" />

        </div>
    );
}

FormDetailStarship.propTypes = propTypes;
FormDetailStarship.defaultProps = defaultProps;

export default FormDetailStarship;
