import produce from "immer";
import useAppDataContext from "../../AppDataProvider/hooks/useAppDataContext";
import useValidationErrors from "../../AppDataProvider/hooks/useValidationErrors";
import LabelInput from "../LabelInput/LabelInput";

const propTypes = {};
const defaultProps = {};

const FormDetailSubmit = () => {
    const { setState } = useAppDataContext();
    const fieldsWithErrorsCount = Object.keys(useValidationErrors()).length;

    return (
        <div className="form-detail-personal form-detail-container">
            {/* TODO: isEqual: firstname + ' ' + lastname */}
            <LabelInput field="signature" label="Signature" />

            {/* TODO: isEqual: Yes */}
            <LabelInput field="agreeTermsOfUse" label="Agree Terms Of Use" />

            {/* TODO: isAfter 2 and isBefore 12 */}
            <LabelInput field="notDroid" label="Not Droid" />

            <button onClick={() => {
                if (fieldsWithErrorsCount) {
                    alert('Do not use alert!!!');
                } else {
                    setState(produce(draftState => void (draftState.employeeOriginal = {...draftState.employee})));
                }
            }}>Submit</button>
        </div>
    );
}

FormDetailSubmit.propTypes = propTypes;
FormDetailSubmit.defaultProps = defaultProps;

export default FormDetailSubmit;
