import './LabelValue.css';
import PropTypes from "prop-types";

const propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
};
const defaultProps = {};

const LabelValue = ({ label, value }) => {
    return (
        <div className="label-value__container">
            <div className="label-value__label">{label}:</div>
            <div className="label-value__value">{value}</div>
        </div>
    );
}

LabelValue.propTypes = propTypes;
LabelValue.defaultProps = defaultProps;

export default LabelValue;
