import FormDetail from "../FormDetail/FormDetail";
import FormSummary from '../FormSummary/FormSummary';
import './Form.css';

const propTypes = {};
const defaultProps = {};

const Form = () => {
    return (
        <div className="content-container">
            <div className="form-container flex flex-column">
                <FormSummary />
                <FormDetail />
            </div>
        </div>
    );
}

Form.propTypes = propTypes;
Form.defaultProps = defaultProps;

export default Form;
