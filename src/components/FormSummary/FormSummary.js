import useAppDataContext from "../../AppDataProvider/hooks/useAppDataContext";
import useDirtyStatus from "../../AppDataProvider/hooks/useDirtyStatus";
import useValidationErrors from "../../AppDataProvider/hooks/useValidationErrors";
import LabelValue from '../LabelValue/LabelValue';

const propTypes = {};
const defaultProps = {};

const FormSummary = () => {
    const { state: { employee } } = useAppDataContext();
    const fieldsWithErrorsCount = Object.keys(useValidationErrors()).length;

    const dirtyStatuses = useDirtyStatus();
    const dirtyFields = Object.keys(employee).filter(field => employee[field] !== dirtyStatuses[field]);
    const dirtyFieldsDirtyCount = dirtyFields.length;

    return (
        <div className="form-summary">
            <h2 className="form-summary__title">Summary</h2>
            <div className="form-summary__content flex flex-row flex-wrap justify-around">
                <LabelValue label="First Name" value={employee.firstName} />
                <LabelValue label="Last Name" value={employee.lastName} />
                <LabelValue label="Planet" value={employee.planet} />
                <LabelValue label="Job" value={employee.job} />
                <LabelValue label="Starship Name" value={employee.starship.name} />
                <LabelValue label="Employment Date" value={employee.employmentStartDate} />
                <LabelValue label="Am I Valid?" value={fieldsWithErrorsCount ? "HAVE ERRORS!!!" : "All Good"}/>
                <LabelValue label="Am I Dirty?" value={dirtyFieldsDirtyCount ? "YES! VERY DIRTY!" : "All Clean"}/>

                {/* TODO: show if valid or not */}
                {/* TODO: show if dirty or not */}
            </div>
        </div>
    );
}

FormSummary.propTypes = propTypes;
FormSummary.defaultProps = defaultProps;

export default FormSummary;
