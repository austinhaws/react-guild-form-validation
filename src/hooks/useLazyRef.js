import { isFunction } from 'lodash-es';
import { useRef } from 'react'

const useLazyRef = defaultValue => {
    const ref = useRef();
    if (!ref.current) {
        ref.current = isFunction(defaultValue) ? defaultValue() : defaultValue;
    }
    return ref;
}

export default useLazyRef;
