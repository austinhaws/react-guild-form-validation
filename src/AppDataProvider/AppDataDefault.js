import Tabs from "../enum/Tabs";

const AppDataDefault = {
    formState: {
        formDetailTab: Tabs.personal,
        isDirty: false,
        isValid: [],
    },

    // stores current valid state...
    validationIsValid: null,
    validationErrors: {
        firstName: [],
        middleName: [],
        lastName: [],
        age: [],
        birthDate: [],
        race: [],
        isSith: [],
        isJedi: [],
        ssn: [],
        email: [],
        addressLine1: [],
        addressLine2: [],
        city: [],
        sector: [],
        planet: [],
        planetCoordinates: [],
        job: [],
        creditsPerHour: [],
        employer: [],
        employmentStartDate: [],
        webUrl: [],
        starship: {
            name: [],
            type: [],
            armament: [],
            crewSize: [],
            length: [],
            parsecsForKesselRun: [],
        },
        signature: [],
        agreeTermsOfUse: [],
        notDroid: [],
    },

    // employee data - see the commented out employee below this for blank values
    employee: {
        // personal information
        firstName: 'Han',
        middleName: 'Cadet 124-329',
        lastName: 'Solo',
        age: '52',
        birthDate: '03/01/5201',
        race: 'Human',
        isSith: 'No',
        isJedi: 'No',
        ssn: '555-12-0298',

        // contact information
        email: 'hsolo@utah.gov',
        addressLine1: '3rd street',
        addressLine2: 'Apt C',
        city: 'Coronet City',
        sector: 'Corellian',
        planet: 'Corellia',
        planetCoordinates: 'M-11',

        // career information
        job: 'Scoundrel',
        creditsPerHour: '32.12',
        employer: 'Self-Employed',
        employmentStartDate: '03/01/5201',
        webUrl: 'https://starwars.fandom.com/wiki/Han_Solo',

        // starship information
        starship: {
            name: 'Millennium Falcon (YT 492727ZED, aka Stellar Envoy)',
            type: 'Corellian YT-1300f light freighter',
            armament: 'two CEC AG-2G quad laser cannons (one ventral and one dorsal), enhanced laser actuators, gas feeds',
            crewSize: '3',
            length: '34.75m',
            parsecsForKesselRun: '12.14',
        },

        // bookkeeping
        signature: 'Han Solo',
        agreeTermsOfUse: 'yes',
        notDroid: '5',
    },

    // A copy of the employee before edits are made, could be used for tracking "dirty" state
    employeeOriginal: {
        firstName: '',
        middleName: '',
        lastName: '',
        age: '',
        birthDate: '',
        race: '',
        isSith: '',
        isJedi: '',
        ssn: '',
        email: '',
        addressLine1: '',
        addressLine2: '',
        city: '',
        sector: '',
        planet: '',
        planetCoordinates: '',
        job: '',
        creditsPerHour: '',
        employer: '',
        employmentStartDate: '',
        webUrl: '',
        starship: {
            name: '',
            type: '',
            armament: '',
            crewSize: '',
            length: '',
            parsecsForKesselRun: '',
        },
        signature: '',
        agreeTermsOfUse: '',
        notDroid: '',
    },
    // employee: {
    //     // personal information
    //     firstName: '',
    //     middleName: '',
    //     lastName: '',
    //     age: '',
    //     birthDate: '',
    //     race: '',
    //     isSith: '',
    //     isJedi: '',
    //     ssn: '',

    //     // contact information
    //     email: '',
    //     addressLine1: '',
    //     addressLine2: '',
    //     city: '',
    //     sector: '',
    //     planet: '',
    //     planetCoordinates: '',

    //     // career information
    //     job: '',
    //     creditsPerHour: '',
    //     employer: '',
    //     employmentStartDate: '',
    //     webUrl: '',

    //     // starship information
    //     starship: {
    //         name: '',
    //         type: '',
    //         armament: '',
    //         crewSize: '',
    //         length: '',
    //         parsecsForKesselRun: '',
    //     },

    //     // bookkeeping
    //     signature: '',
    //     agreeTermsOfUse: '',
    //     notDroid: '',
    // },
};

export default AppDataDefault;
