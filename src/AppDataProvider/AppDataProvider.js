import produce from "immer";
import PropTypes from "prop-types";
import { useEffect, useState } from "react";
import useLazyRef from "../hooks/useLazyRef";
import dirtyService from "../service/dirtyService";
import AppDataContext from "./AppDataContext";
import AppDataDefault from "./AppDataDefault";
import yupSchema from "./createYupSchema";

const propTypes = {
    children: PropTypes.node.isRequired,
};
const defaultProps = {};

const AppDataProvider = ({ children }) => {
    // see lucid chart for visual data break out
    // https://lucid.app/lucidspark/618ae371-1ce7-4eb4-a47d-c0cc582a5472/edit#
    const [state, setState] = useState(() => (
        produce(AppDataDefault, draftData => void(draftData.employeeOriginal = dirtyService.setCleanEmployee(AppDataDefault.employee)))
    ));
    
    useEffect(() => {
        if (state?.employee) {
            yupSchema.validate(
                state.employee,
                { abortEarly: false }
            )
                .then(result => {
                    setState(produce(draftState => {
                        draftState.validationIsValid = true;

                        // set all to valid
                        draftState.validationErrors = { ...AppDataDefault.validationErrors };
                    }));
                })
                .catch(errors => {
                    setState(produce(draftState => {
                        draftState.validationIsValid = false;

                        // set all fields to default no errors
                        draftState.validationErrors = { ...AppDataDefault.validationErrors };

                        // set errors for each field out of compliance
                        errors.inner.forEach(error => draftState.validationErrors[error.params.path] = error.errors)
                    }));
                });
        }
    }, [state?.employee]);

    return (
        <AppDataContext.Provider
            value={
                {
                    state,
                    setState,
                    onFieldChange: (field, newValue) => {
                        setState(produce(draftState => {
                            draftState.employee[field] = newValue;
                        }));
                    },
                    onFieldChangeCurry: field => newValue => {
                        setState(produce(draftState => {
                            draftState.employee[field] = newValue;
                        }));
                    },
                }
            }
        >
            {children}
        </AppDataContext.Provider>
    );
}

AppDataProvider.propTypes = propTypes;
AppDataProvider.defaultProps = defaultProps;

export default AppDataProvider;
