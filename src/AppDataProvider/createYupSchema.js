import * as yup from "yup";

const yupSchema =
    yup.object()
        .shape({
            firstName: yup.string().matches(/^[a-z|0-9]+$/).required(),
            lastName: yup.string().matches(/^[a-z|0-9]+$/i).required(),
            age: yup.number().required().positive().integer(),
            // email: yup.string().email(),
            // website: yup.string().url(),
            // createdOn: yup.date().default(function () {
            //     return new Date();
            // }),
            job: yup.string().required(),
            creditsPerHour: yup.number().positive().integer('must be a number').required().label('Credit Hours'),
            employer: yup.string().required(),
            employmentStartDate: yup.date().required(),
            webUrl: yup.string().url().required(),

            starship: yup.object({
                name: yup.string().matches(/^[a-z|0-9]+$/).required(),
            }),
        });

export default yupSchema;
