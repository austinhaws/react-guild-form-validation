import { isArray } from "lodash-es";
import objectAtPath from "../../util/objectAtPath";
import useAppDataContext from "./useAppDataContext";

/**
 *
 * @param {array|string|undefined} fields if string then is errors for that field; if array then is object with just those field errors; else all errors
 * @returns
 */
const useValidationErrors = fields => {
    const errors = useAppDataContext().state.validationErrors;

    let result;
    let shouldFilter = true;
    if (isArray(fields)) {
        result = Object.fromEntries(fields.map(field => [field, errors[field]]));
    } else if (fields) {
        result = errors[fields];
        // SNOWFLAKE ALERT!!!
        shouldFilter = false;
    } else {
        result = errors;
    }

    if (shouldFilter) {
        result = (
            Object.fromEntries(
                Object.entries(result)
                    .filter(([key, value]) => value && value.length)
            )
        );
    }

    return result;
};
export default useValidationErrors;
