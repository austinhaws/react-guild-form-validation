import { useContext } from "react";
import AppDataContext from "../AppDataContext";

const useAppDataContext = () => useContext(AppDataContext);
export default useAppDataContext;
