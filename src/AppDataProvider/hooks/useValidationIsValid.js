import useAppDataContext from "../useAppDataContext";

const useValidationIsValid = () => useAppDataContext().validationIsValid;
export default useValidationIsValid;
