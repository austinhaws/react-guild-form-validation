import { isArray } from "lodash-es";
import useAppDataContext from "./useAppDataContext";

/**
 *
 * @param {array|string|undefined} fields if string then is errors for that field; if array then is object with just those field errors; else all errors
 * @returns
 */
const useDirtyStatus = fields => {
    const originalValues = useAppDataContext().state.employeeOriginal;

    let result;
    if (isArray(fields)) {
        result = Object.fromEntries(fields.map(field => [field, originalValues[field]]));
    } else if (fields) {
        result = originalValues[fields];
    } else {
        result = originalValues;
    }
    return result;
};
export default useDirtyStatus;
